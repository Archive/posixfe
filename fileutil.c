/* -*- Mode: C; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 2 -*-
 *
 * The contents of this file are subject to the Netscape Public License
 * Version 1.0 (the "NPL"); you may not use this file except in
 * compliance with the NPL.  You may obtain a copy of the NPL at
 * http://www.mozilla.org/NPL/
 *
 * Software distributed under the NPL is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the NPL
 * for the specific language governing rights and limitations under the
 * NPL.
 *
 * The Initial Developer of this code under the NPL is Netscape
 * Communications Corporation.  Portions created by Netscape are
 * Copyright (C) 1998 Netscape Communications Corporation.  All Rights
 * Reserved.
 */
/* 
   fileutils.c -- Misc. stuff for manipulating files and paths in a
                   posic environment.
   Created: Vidar Hokstad <vidarh@ncg.net>, 1998/04/02
     Mostly cut'n'paste from misc XFE files currently...
 */





#include "mozilla.h"
#include "name.h"
#include "xp.h"
#include "xp_help.h" /* for XP_NetHelp() */
#include "xp_sec.h"
#include "np.h"

#ifdef __sgi
# include <malloc.h>
#endif

#ifdef MOZILLA_GPROF
#include "gmon.h"
#endif

/* for XP_GetString() */
#include <xpgetstr.h>

/*extern int FE_MOZILLA_NOT_FOUND_IN_PATH;*/
#define FE_MOZILLA_NOT_FOUND_IN_PATH 0

const char *fe_progname_long;
const char *fe_progname;
const char *fe_progclass;

#ifdef DEBUG
int Debug;
#endif

#if defined(NSPR) && defined(TOSHOK_FIXES_THE_SPLASH_SCREEN_HANGS)
#define NSPR_SPLASH
#endif

#define Boolean int

Boolean
fe_is_absolute_path(char *filename)
{
	if ( filename && *filename && filename[0] == '/')
		return True;
	return False;
}

Boolean
fe_is_working_dir(char *filename, char** progname)
{
   *progname = 0;

   if ( filename && *filename )
   {
	if ( (int)strlen(filename)>1 && 
		   filename[0] == '.' && filename[1] == '/')
	{
          char *str;
          char *name;

	  name = filename;

	  str = strrchr(name, '/');
	  if ( str ) name = str+1;

	  *progname = (char*)malloc((strlen(name)+1)*sizeof(char));
	  strcpy(*progname, name);

	  return True;
	}
	else if (strchr(filename, '/'))
	{
	  *progname = (char*)malloc((strlen(filename)+1)*sizeof(char));
	  strcpy(*progname, filename);
	  return True;
	}
    }
    return False;
}

Boolean 
fe_found_in_binpath(char* filename, char** dirfile)
{
    char *binpath = 0;
    char *dirpath = 0;
    struct stat buf;

    *dirfile = 0;
    binpath = getenv("PATH");

    if ( binpath )
    {
    	binpath = XP_STRDUP(binpath);
	dirpath = XP_STRTOK(binpath, ":");
        while(dirpath)
        {
	   if ( dirpath[strlen(dirpath)-1] == '/' )
           {
	        dirpath[strlen(dirpath)-1] = '\0';
           }

	   *dirfile = PR_smprintf("%s/%s", dirpath, filename);

 	   if ( !stat(*dirfile, &buf) )
           {
	   	XP_FREE(binpath);
		return True;
           }
	   dirpath = XP_STRTOK(NULL,":");
	   XP_FREE(*dirfile);
	   *dirfile = 0;
        }
	XP_FREE(binpath);
    }
    return False;
}

char *
fe_expand_working_dir(char *cwdfile)
{
   char *dirfile = 0;
   char *string;
#if defined(SUNOS4)||defined(AIX)
   char path [MAXPATHLEN];
#endif

#if defined(SUNOS4)||defined(AIX)
   string = getwd (path);
#else
   string = getcwd(NULL, MAXPATHLEN);
#endif

   dirfile = (char *)malloc((strlen(string)+strlen("/")+strlen(cwdfile)+1) 
			*sizeof(char));
   strcpy(dirfile, string);
   strcat(dirfile,"/");
   strcat(dirfile, cwdfile);
#if !(defined(SUNOS4) || defined(AIX))
   XP_FREE(string);
#endif
   return dirfile;
}


/****************************************
 * This function will return either of these
 * type of the exe file path:
 * 1. return FULL Absolute path if user specifies a full path
 * 2. return FULL Absolute path if the program was found in user 
 *    current working dir 
 * 3. return relative path (ie. the same as it is in fe_progname)
 *    if program was found in binpath
 *
 ****************************************/
const char *
fe_locate_program_path(const char *fe_prog)
{
  char *ret_path = 0;
  char *dirfile = 0;
  char *progname = 0;

  StrAllocCopy(progname, fe_prog);

  if ( fe_is_absolute_path(progname) )
  {
	StrAllocCopy(ret_path, progname);
        XP_FREE(progname);
	return ret_path;
  }
  else if ( fe_is_working_dir(progname, &dirfile) )
  {
	ret_path = fe_expand_working_dir(dirfile);
        XP_FREE(dirfile);
        XP_FREE(progname);
        return ret_path;
  }
  else if ( fe_found_in_binpath(progname, &ret_path) )
  {
        if ( fe_is_absolute_path(ret_path) )
        {
	   /* Found in the bin path; then return only the exe filename */
	   /* Always use bin path as the search path for the file 
	      for consecutive session*/
	   XP_FREE(ret_path);
	   ret_path = progname;
       	   XP_FREE(dirfile);
	}
	else if (fe_is_working_dir(ret_path, &dirfile) )
	{
		XP_FREE(ret_path);
		XP_FREE(progname);
		ret_path = fe_expand_working_dir(dirfile);
        	XP_FREE(dirfile);
	}
	return ret_path;
  }
  else
  {
      XP_FREE(ret_path);
      XP_FREE(dirfile);
      XP_FREE(progname);

      fprintf(stderr,
			  XP_GetString(FE_MOZILLA_NOT_FOUND_IN_PATH),
			  fe_progname);
    
      exit(-1);
  }
}

/****************************************
 * This function will return either of these
 * type of the file path:
 * 1. return FULL Absolute path if user specifies a full path
 * 2. return FULL Absolute path if the program was found in user 
 *    current working dir 
 * 3. return relative path (ie. the same as it is in fe_progname)
 *    if program was found in binpath
 * 4. return NULL if we cannot locate it
 ****************************************/
char *
fe_locate_component_path(const char *fe_prog, char *comp_file)
{
  char *ret_path = 0;
  char *dirfile = 0;
  char *progname = 0;
  char  compname[1024];
  char *separator;
  char *moz_home = 0;
  struct stat buf;

  /* replace the program name with component name first */
  XP_STRCPY(compname, fe_prog);
  separator = XP_STRRCHR(compname, '/');
  if (separator) {
	  XP_STRCPY(separator+1, comp_file);
  }
  else {
	  XP_STRCPY(compname, comp_file);
  }

  StrAllocCopy(progname, compname);

  if ( fe_is_absolute_path(progname) ) {
	  if (! stat(progname, &buf)) {
		  StrAllocCopy(ret_path, progname);
		  XP_FREE(progname);
		  return ret_path;
	  }
  }
  
  if ( fe_is_working_dir(progname, &dirfile) ) {
	  ret_path = fe_expand_working_dir(dirfile);
	  if (! stat(ret_path, &buf)) {
		  XP_FREE(dirfile);
		  XP_FREE(progname);
		  return ret_path;
	  }
  }

  /* Now see if we can find it in the bin path */
  if ( fe_found_in_binpath(comp_file, &ret_path) ) {
	  if ( fe_is_absolute_path(ret_path) ) {
		  /* Found in the bin path; then return only the exe filename */
		  /* Always use bin path as the search path for the file 
			 for consecutive session*/
		  XP_FREE(progname);
		  XP_FREE(dirfile);
		  return ret_path;
	  }
	  else if (ret_path[0] == '.' && ret_path[1] == '/') { /* . relative */
		  char* foo = fe_expand_working_dir(&ret_path[2]);
		  XP_FREE(ret_path);
		  ret_path = foo;
		  XP_FREE(progname);
		  XP_FREE(dirfile);
		  return ret_path;
	  }
  }

  /* Check $MOZILLA_HOME */

  moz_home  = getenv("MOZILLA_HOME");

  if (moz_home) {
	  XP_SPRINTF(compname, "%s/%s", moz_home, comp_file);
	  if (! stat(compname, &buf)) {
		  StrAllocCopy(ret_path, compname);
		  if (dirfile) XP_FREE(dirfile);
		  if (progname) XP_FREE(progname);
		  return ret_path;
	  }
  }

  /* cannot find it */

  if (ret_path) XP_FREE(ret_path);
  if (dirfile) XP_FREE(dirfile);
  if (progname) XP_FREE(progname);
  return NULL;
}

void fe_GetProgramDirectory(char *path, int len)
{
	char * separator;
	char * prog = 0;

	*path = '\0';

	if ( fe_is_absolute_path( (char*)fe_progname_long ) )
		strncpy (path, fe_progname_long, len);
	else
	{
		if ( fe_found_in_binpath((char*)fe_progname_long, &prog) )
		{
			strncpy (path, prog, len);
	   		XP_FREE (prog);
		}
	}

	if (( separator = XP_STRRCHR(path, '/') ))
		separator[1] = 0;
	return;
}
