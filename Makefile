#! gmake
#
# The contents of this file are subject to the Netscape Public License
# Version 1.0 (the "NPL"); you may not use this file except in
# compliance with the NPL.  You may obtain a copy of the NPL at
# http://www.mozilla.org/NPL/
#
# Software distributed under the NPL is distributed on an "AS IS" basis,
# WITHOUT WARRANTY OF ANY KIND, either express or implied. See the NPL
# for the specific language governing rights and limitations under the
# NPL.
#
# The Initial Developer of this code under the NPL is Netscape
# Communications Corporation.  Portions created by Netscape are
# Copyright (C) 1998 Netscape Communications Corporation.  All Rights
# Reserved.
#
# Created:
#    1998/04/02    Vidar Hokstad <vidarh@ncg.net
#

DEPTH		= ../..

MODULE		= posixfe
LIBRARY_NAME	= posixfe

CSRCS		= config.c \
                  pfe-dns.c \
                  fileutil.c


#
# It doesn't really require all of this... I was just lazy, and we might
# need a lot of it later.
#

REQUIRES	= \
		  addr \
		  applet \
		  img \
		  edtplug \
		  jtools \
		  lay \
		  layer \
		  msg \
		  js \
		  plds \
		  nspr20 \
		  parse \
		  plug \
		  hook \
		  pref \
		  rdf \
		  java \
		  security \
		  softupdt \
		  libreg \
		  style \
		  util \
		  xfeicons \
		  $(NULL)
		  

include $(DEPTH)/config/config.mk
include $(DEPTH)/config/rules.mk
